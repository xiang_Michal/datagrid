/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mziarniak.hadoopworldcount;

/**
 *
 * @author xiang
 */
public class EntropyMetter {
    public static double checkEntropy(String text) {
        boolean isUpperCase = false;
        boolean isLowerCase = false;
        boolean haveDigits = false;
        
        final int lowerCaseCharacterRange = 26;
        final int upperCaseCharacterRange = 26;
        final int digitsCharacterRange = 10;
        int charactersRange = 0;
        
        for (char c : text.toCharArray()) {
            if (Character.isUpperCase(c) && !isUpperCase) {
                charactersRange += upperCaseCharacterRange;
                isUpperCase = true;
            }
            if (Character.isLowerCase(c) && !isLowerCase) {
                charactersRange += lowerCaseCharacterRange;
                isLowerCase = true;
            }
            if (Character.isDigit(c) && !haveDigits) {
                charactersRange += digitsCharacterRange;
                haveDigits = true;
            }
        }
       
        /*  E = log2(R) * L, gdzie:
            E - entropia, 
            R - zakres wykorzystanych znakow, 
            L - dlugosc ciagu
        
            log2(n) = log(n) / log(2) 
        */
       return  Math.log(charactersRange) / Math.log(2) * text.length();
    }
}

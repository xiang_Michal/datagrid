/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mziarniak.hadoopworldcount;

/**
 *
 * @author pcjee-6
 */
import com.google.common.math.DoubleMath;
import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.map.InverseMapper;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class WordCount extends Configured implements Tool {

    @Override
    public int run(String[] args) throws Exception {
    Configuration conf = this.getConf();
    Job job = Job.getInstance(conf, "entropy");
    job.setJarByClass(WordCount.class);
    
    job.setMapperClass(TokenizerMapper.class);
    job.setCombinerClass(EntropyReducer.class);
    
    job.setReducerClass(EntropyReducer.class);
    job.setSortComparatorClass(LongWritable.DecreasingComparator.class);    
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(DoubleWritable.class);

    
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    
    job.waitForCompletion(true);
    
    Job job2 = Job.getInstance(conf, "sort it");
    job2.setJarByClass(WordCount.class);
    job2.setMapperClass(SortMapper.class);
    job2.setSortComparatorClass(SortDoubleComparator.class);
    job2.setCombinerClass(SortReducer.class);
    job2.setReducerClass(SortReducer.class);
    
    
    
    job2.setOutputKeyClass(DoubleWritable.class);
    job2.setOutputValueClass(Text.class);
    FileInputFormat.addInputPath(job2, new Path(args[1]));
    FileOutputFormat.setOutputPath(job2, new Path(args[2]));    
   
    
    return job2.waitForCompletion(true) ? 0 : 1;
    }

  public static class TokenizerMapper
       extends Mapper<Object, Text, Text, DoubleWritable>{

    private final static DoubleWritable one = new DoubleWritable(1.0);
    private Text word = new Text();

    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      StringTokenizer itr = new StringTokenizer(value.toString());
      while (itr.hasMoreTokens()) {
        word.set(itr.nextToken());
        context.write(word, one);
      }
    }
  }

  public static class EntropyReducer
       extends Reducer<Text,Text,Text,DoubleWritable> {
    private DoubleWritable result = new DoubleWritable();

    public void reduce(Text key, Iterable<Text> values,
                       Context context
                       ) throws IOException, InterruptedException {
        result.set(EntropyMetter.checkEntropy(key.toString()));    
        context.write(key, result);
    }
  }
  
  
  public static class SortMapper extends Mapper<Object, Text, DoubleWritable, Text>{
    private DoubleWritable entropy = new DoubleWritable(0);
    private Text pass = new Text();

    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      StringTokenizer itr = new StringTokenizer(value.toString());
      while (itr.hasMoreTokens()) {
        pass.set(itr.nextToken());
        entropy.set(Double.valueOf(itr.nextToken()));
        context.write(entropy,pass);
      }
    
    }
  }

  
  public static class SortReducer extends Reducer<DoubleWritable, Text, DoubleWritable, Text>{
    public void reduce(DoubleWritable key, Iterable<Text> values,
                       Context context
                       ) throws IOException, InterruptedException {
      for (Text val : values) {
        context.write(key,val);
      }
     
      
    }
    }
  


    
  public static void main(String[] args) throws Exception {
      
      int res = ToolRunner.run(new Configuration(), new WordCount(), args);
        System.exit(res);
        
 
  }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mziarniak.hadoopspring;
import com.mziarniak.passwordentropy.PasswordEntropy;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.util.ToolRunner;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 *
 * @author xiang
 */
@RestController
@RequestMapping("/")
public class HadoopController {
    
    
    String pathInput = "hdfs://localhost:9000/user/xiang/input";
    String pathOutput = "hdfs://localhost:9000/user/xiang/output";
    String pathOutput2 = "hdfs://localhost:9000/user/xiang/output2";

 @RequestMapping(value = "/")
 public String main() {
  String result="<a href='/start'>Start</a> <br> <p>Input: "+pathInput+"</p>";  
  return result;
 }
	
 @RequestMapping(value = "/start")
 public String run() throws Exception {

	 String[] arg = {pathInput, pathOutput,pathOutput2};
	 int res = ToolRunner.run(new Configuration(), new PasswordEntropy(), arg);
	 

	 String result="Starts"+ "<br>"; 
     
  return result;
 }
 
 @RequestMapping(value = "/getresult")
 public String getResult() {
     
  String result="show results";  
  
  Configuration conf = new Configuration();
    conf.set("fs.defaultFS", "hdfs://localhost:9000");
    FileSystem fs;
        try {
            fs = FileSystem.get(conf);
           
     
            FileStatus[] status = fs.listStatus(new Path("hdfs://localhost:9000/user/xiang/output2/"));
            for (int i=0;i<status.length;i++){
                FSDataInputStream open = fs.open(status[i].getPath());
               while(open.readLine()!=null){
                   result=result+"<br>"+open.readLine();
               }
                
                
            }
            
            
        } catch (IOException ex) {
            Logger.getLogger(HadoopController.class.getName()).log(Level.SEVERE, null, ex);
        }
    
 
  return result;
 }
}